#include "listservice.h"
#include <algorithm>

const int ERROR = -1;

listService::listService()
{
}

bool listService::addScientist(computerScientist& entry)
{
    bool isSuccess = repository.addScientist(entry);
    return isSuccess;
}

bool listService::addComputer(computers& entry)
{
    bool isSuccess = repository.addComputer(entry);
    return isSuccess;
}

void listService::remove(const string& type, const int& ID)
{
    repository.restoreOrDelete(type, ID, true);
}

void listService::editScientist(const computerScientist& entry)
{
    computerScientist temp = entry;
    string name, gender;
    int birthYear, deathYear, ID;

    name = temp.getName();
    gender = temp.getGender();
    birthYear = temp.getBirthYear();
    deathYear = temp.getDeathYear();
    ID = temp.getID();

    repository.updateScientist(ID, name, gender, birthYear, deathYear);
}

void listService::editComputer(const computers& entry)
{
    computers temp = entry;
    string name, type;
    int buildYear, ID;
    bool wasBuilt;

    name = temp.getName();
    type = temp.getType();
    buildYear = temp.getBuildYear();
    ID = temp.getID();
    wasBuilt = temp.getWasBuilt();

    repository.updateComputer(ID, name, type, buildYear, wasBuilt);
}

void listService::searchResults(const string& input)
{
    vector<string> searchInput = splitString(input);

    repository.findInRepository(searchInput);
}

void listService::SearchByColumn(const string& input, const string& tableCol)
{
    repository.findByColumn(input,tableCol);
}

void listService::addRelation(const int& scientistID, const int& computerID)
{
    repository.addRelation(scientistID, computerID);
}

void listService::removeRelation(const int& scientistID, const int& computerID)
{
    repository.removeRelation(scientistID, computerID);
}

vector<computerScientist> listService::getScientistList()
{
    return repository.getScientistList();
}

vector<computers> listService::getComputerList()
{
    return repository.getComputerList();
}

void listService::scientistRelations(const int& scientistID)
{
    repository.scientistRelation(scientistID);
}

void listService::computerRelations(const int& computerID)
{
    repository.computerRelation(computerID);
}

void listService::getSortedList(const string& type ,const int& display_case)
{
    string colOne = "name", colTwo = "gender", colThree = "birthYear", colFour = "deathYear";

    if(type == "computer")
    {
        colTwo = "type";
        colThree = "buildYear";
        colFour = "wasBuilt";
    }

    switch(display_case)
    {
    case 1:
        repository.sort(type, colOne);
        break;
    case 2:
        repository.sort(type, colTwo);
        break;
    case 3:
        repository.sort(type, colThree);
        break;
    case 4:
        repository.sort(type, colFour);
        break;
    }
}

vector<string> splitString(const string& str)
{
    vector<string> temp;
    string vectorInput = "";
    int stringLength = str.length();
    bool wasLastSpace = true;

    for(int i = 0; i < stringLength; i++)
    {
        if(isspace(str[i]))
        {
            if(wasLastSpace == false)
            {
                temp.push_back(vectorInput);
                vectorInput = "";
            }
            wasLastSpace = true;
            continue;
        }

        wasLastSpace = false;
        vectorInput += str[i];
    }

    if(vectorInput.length() != 0)
        temp.push_back(vectorInput);

    return temp;
}

bool isSubString(const string& str, const string& subStr)
{
    vector<string> stringSplit = splitString(str);
    int size = stringSplit.size();

    for(int i = 0; i < size; i++)
    {
        if(subStr == stringSplit[i])
            return true;
    }

    return false;
}

