#include "computerscientist.h"

const int notAvailible = -1;

computerScientist::computerScientist()
{
    name = "";
    gender = "N/A";
    birthYear = notAvailible;
    deathYear = notAvailible;
}

computerScientist::computerScientist(const string& sName, const string& sGender, const int& bYear, const int& dYear, const int& sID, const bool& tIsDeleted)
{
    edit(sName, sGender, bYear, dYear);
    ID = sID;
    deleted = tIsDeleted;
}

void computerScientist::edit(const string& sName, const string& sGender, const int& sBirthYear, const int& sDeathYear)
{
    name = sName;
    gender = sGender;
    birthYear = sBirthYear;
    deathYear = sDeathYear;
}

string computerScientist::getName()
{
    return name;
}

string computerScientist::getGender()
{
    return gender;
}

int computerScientist::getBirthYear()
{
    return birthYear;
}

int computerScientist::getDeathYear()
{
    return deathYear;
}

int computerScientist::getID()
{
    return ID;
}

bool computerScientist::isDeleted()
{
    return deleted;
}
