#ifndef COMPUTERS_H
#define COMPUTERS_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class computers
{

public:
    computers();
    computers(const string &name, const string &type, const int &buildYear, const bool &wasBuilt, const int &cID, const bool &isDeleted);
    string getName();
    int getBuildYear();
    string getType();
    void edit(const string &c_name, const int &c_buildYear, const string &c_type, const bool &c_wasBuilt);
    int getID();
    bool isDeleted();
    bool getWasBuilt();
private:
    string name;
    string type;
    int buildYear;
    bool wasBuilt;
    int ID;
    bool deleted;
};

#endif // COMPUTERS_H
