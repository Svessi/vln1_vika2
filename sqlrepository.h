#ifndef SQLREPOSITORY_H
#define SQLREPOSITORY_H

#include <iostream>
#include <string>
#include <vector>
#include <QtSql>
#include <sstream>
#include <QString>
#include "computerscientist.h"
#include "computers.h"

class sqlrepository
{
public:
    sqlrepository();
    ~sqlrepository();
    vector<computerScientist> getScientistList();
    vector<computers> getComputerList();
    void writeToDatabase();
    bool addScientist(computerScientist entry);
    bool addComputer(computers entry);
    void findInRepository(const vector<string> &input);
    void findByColumn(const string &input, const string &tableCol);
    void findScientistInRepository(computerScientist sci);
    void findComputerInRepository(computers comp);
    void restoreOrDelete(const string& type, const int &ID, const bool &isDelete);
    void updateScientist(const int &ID, const string &name, const string &gender, const int &birthY, const int &deathY);
    void updateComputer(const int &ID, const string &name, const string &type, const int &buildY, const bool &wasBuilt);
    void sort(const string &tableName, const string &colName);
    void scientistRelation(int ID);
    void computerRelation(int ID);
    void addRelation(const int &scientistID, const int &computerID);
    void removeRelation(const int &scientistID, const int &computerID);
private:
    vector<computerScientist> scientistList;
    vector<computers> computerList;
    QString dbName;
    QSqlDatabase database;
    void readSientistsFromDatabase(const string &command);
    void readComputersFromDatabase(const string &command);
    void insertScientist(computerScientist entry);
    void insertComputer(computers entry);
};

string IntToString(const int& integer);
void addWildCarts(vector<string>& list);
vector<string> getAllScientistCol();
vector<string> getAllComputerCol();
string makeSearchCommand(const string& type, const vector<string>& col, const vector<string>& search);

#endif // SQLREPOSITORY_H
