#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "computerscientist.h"
#include "computers.h"
#include "listservice.h"

using namespace std;

class consoleUI
{
public:
    consoleUI();
    void start();
private:
    listService serviceLayer;
    vector<string> commandList;
    void initCommandList();
    void printCommandDescpriptions();
    void processCommand(const string &command);
    void processAdd();
    void processRemoveOrEdit(string RemoveOrEdit);
    void processSearch();
    void processDisplay();
    void addScientist();
    void addComputer();
    void removeEntry(const string &type, const int &ID);
    void processRelation();
    void addRelationToScientist(const int &sciID);
    void addRelationToComputer(const int &compID);
    void viewComputerRelations(computers comp);
    void viewScientistsRelations(computerScientist sci);
};

void makeScientist(computerScientist& entry);
void makeComputer(computers& entry);
bool makeYear(int& year);
string formatString(const string& str, const bool& isFormatUpperLower);
bool printListOfScientist(vector<computerScientist> &list);
bool printListOfComputer(vector<computers>& list);
bool isNameValid(const string& name, const bool& allowDigits, const bool& allowPunct);
bool isYearValid(const string& year);
string askForName(const bool &allowDigits, const bool &allowPunct);
string askForGender();
string askForType();
int askForBirthYear();
int askForDeathYear(const int& birthYear);
int askForBuildYear();
bool askForWasBuilt();
void editScientist(computerScientist& entry);
void editComputer(computers& entry);
void editScientistInfo(string& name, string& gender, int& birthYear, int& deathYear);
void editComputerInfo(string& name, string& type, int& buildYear, bool& wasBuilt);
ostream& operator << (ostream& out, computerScientist a);
ostream& operator << (ostream& out, computers c);

#endif // CONSOLEUI_H

