#include <iostream>
#include "consoleui.h"

using namespace std;

ostream& operator << (ostream& out, computerScientist a);

int main()
{
    consoleUI interface;
    interface.start();
}
