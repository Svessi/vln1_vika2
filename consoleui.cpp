#include "consoleui.h"


const int ERROR = -1;

consoleUI::consoleUI()
{
    initCommandList();
}

void consoleUI::start()
{
    string command;

    cout << "Welcome to the incomprehensive list of famous computers and computer scientists.\n"
         << "To continue write one of the following commands. \n\n";

    printCommandDescpriptions();

    do
    {

        command = "";

        cout << "command: ";
        getline(cin,command);
        command = formatString(command, false);

        processCommand(command);

    }while(command != "quit");
}

void consoleUI::initCommandList()
{
    ifstream fin;
    string command;
    char character;

    fin.clear();

    fin.open("commands.txt", ios::in);

    if(fin.fail())
        exit(1);

    if(fin.is_open())
    {

       while(!fin.eof())
       {
           command = "";

           fin.get(character);

           if(character == '*')
           {
               fin.get(character);

               while(character != '*' && !fin.eof())
               {
                   command += character;
                   fin.get(character);
               }

               commandList.push_back(command);
           }
       }
    }
    fin.close();
}

void consoleUI::printCommandDescpriptions()
{
    ifstream fin;
    string description = "";
    char character;

    fin.open("commands.txt", ios::in);

    if(fin.fail())
        exit(1);

    else if(fin.is_open())
    {
        while(!fin.eof())
        {
            fin.get(character);

            if(character != '*')
                description += character;
        }
    }

    fin.close();

    cout << endl << description << endl;
}

void consoleUI::processCommand(const string& command)
{
    if(command == commandList[0])//add
        processAdd();

    else if(command == commandList[1]) //remove
        processRemoveOrEdit("remove");

    else if(command == commandList[2]) //edit
        processRemoveOrEdit("edit");

    else if(command == commandList[3]) //search
        processSearch();

    else if(command == commandList[4]) //display
        processDisplay();

    else if(command == commandList[5])//relation
        processRelation();

    else if(command != "" && command != commandList[6])
    {
        cout << "\nThe program did not recognize the command: " << command << endl << endl;
        return;
    }

    if(command != "" && command != commandList[6])
    {
        string input;
        cout << "Press Enter to return to Menu\n\n";
        getline(cin, input);
        system("cls");
        printCommandDescpriptions();
    }
}

void consoleUI::processAdd()
{
    string input;

    cout << "\nWould you like to add a scientist or a computer to the list?\n\n";
    cout << "\t1:scientist\n\t2:computer\n\tanything else to cancel\n";
    cout << "add: ";
    getline(cin,input);
    input = formatString(input, true);

    if (input == "Scientist" || input == "1")
        addScientist();

    else if (input == "Computer" || input == "2")
        addComputer();
}

void consoleUI::addScientist()
{
    string input;
    computerScientist entry;
    bool success;

    makeScientist(entry);

    cout << "\nYou have created the following entry:\n\n";
    cout << entry << endl;
    cout << "Do you want to add it to the list (Y/N)?: ";
    getline(cin,input);

    if(input == "y" || input == "Y")
    {
        success = serviceLayer.addScientist(entry);

        if(success)
            cout << "\nThe scientist has been successfully added to the list.\n\n";
        else
            cout << "\nThis scientist is already in the list.\n\nAdding operation aborted.\n\n";
    }

    else
        cout << "\nEntry discarded.\n\n";
}

void consoleUI::addComputer()
{
    string input;
    computers entry;
    bool success;

    makeComputer(entry);

    cout << "\nYou have created the following entry:\n\n";
    cout << entry << endl;
    cout << "Do you want to add it to the list (Y/N)?: ";
    getline(cin,input);

    if(input == "y" || input == "Y")
    {
        success = serviceLayer.addComputer(entry);

        if(success)
            cout << "\nThe computer has been successfully added to the list.\n\n";
        else
            cout << "\nThis computer is already in the list.\n\nAdding operation aborted.\n\n";
    }

    else
        cout << "\nEntry discarded.\n\n";
}

void consoleUI::processRemoveOrEdit(string RemoveOrEdit)
{
    string input;
    vector<computerScientist> scientistList;
    vector<computers> computerList;
    int sciSize, compSize;

    cout << "\nPlease enter the name (or a part of the name) of the scientist/computer\nyou want to " << RemoveOrEdit << "\n\n";
    cout << "Name: ";
    getline(cin,input);

    if(input == "")
    {
        cout << "\nNo scientist/computer found.\n\n";
        return;
    }

    input = formatString(input, true);

    serviceLayer.SearchByColumn(input, "name");
    scientistList = serviceLayer.getScientistList();
    computerList = serviceLayer.getComputerList();

    sciSize = scientistList.size();
    compSize = computerList.size();

    if(sciSize || compSize)
    {
        cout << endl << sciSize << " scientists and " << compSize << " computers have been found by that name.\n\n";
        cout << "Write \"" << RemoveOrEdit << "\" to " << RemoveOrEdit << " the current entry\nanything else will fetch the next entry.\n\n";
    }

    else
        cout << "\nNo scientist/computer found.\n\n";

    for(int i = 0; i < sciSize; i++)
    {
        cout << "-----------------------------\n";
        cout << "\tScientist\n\n";
        cout << scientistList[i];
        cout << "-----------------------------\n";
        cout << ": ";
        getline(cin, input);
        input = formatString(input, true);
        cout << endl;

        if(input == formatString(RemoveOrEdit, true))
        {
            if(RemoveOrEdit == "remove")
            {
                serviceLayer.remove("computerScientist",scientistList[i].getID());
                cout << "\nThe scientist has been successfully removed from the list.\n\n";
            }
            else if(RemoveOrEdit == "edit")
            {
                computerScientist temp = scientistList[i];
                editScientist(temp);
                serviceLayer.editScientist(temp);
                cout << "\nEditing done.\n\n";
            }
        }
    }

    for(int i = 0; i < compSize; i++)
    {
        cout << "-----------------------------\n";
        cout << "\tComputer\n\n";
        cout << computerList[i];
        cout << "-----------------------------\n";
        cout << ": ";
        getline(cin, input);
        input = formatString(input,true);

        if(input == formatString(RemoveOrEdit, true))
        {
            if(RemoveOrEdit == "remove")
            {
                serviceLayer.remove("computer",computerList[i].getID());
                cout << "\nThe computer has been successfully removed from the list.\n\n";
            }
            else if(RemoveOrEdit == "edit")
            {
                computers temp = computerList[i];
                editComputer(temp);
                serviceLayer.editComputer(temp);
                cout << "\nEditing done.\n\n";
            }
        }
    }
}

void consoleUI::processSearch()
{
    string input, copyInput;
    vector<computerScientist> scientistList;
    vector<computers> computersList;

    int scientistListSize, computerListSize;
    bool didScientistPrint, didComputerPrint;

    cout << "\nWhat do you want to search for?\n\n";
    cout << "Search: ";
    getline(cin, input);

    copyInput = input;
    input = formatString(input, true);

    serviceLayer.searchResults(input);
    scientistList = serviceLayer.getScientistList();
    computersList = serviceLayer.getComputerList();

    scientistListSize = scientistList.size();
    computerListSize = computersList.size();

    if(scientistListSize || computerListSize)
        cout << "\nYour search result:\n";

    didScientistPrint = printListOfScientist(scientistList);
    didComputerPrint = printListOfComputer(computersList);

    if(!(didScientistPrint || didComputerPrint))
        cout << "Your search -" << copyInput << "- did not match any scientist/computer.\n\n";
}

void consoleUI::processDisplay()
{
    string type, optionOne = "alphabetical",optionTwo,optionThree,optionFour,input = "";
    int displayCase = 0;
    bool didPrint, valid = true;

    do
    {
        valid = true;

        cout << "\nWitch list do you want to display?\n\n";
        cout << "\t1:scientists\n\t2:computers\n\n";
        cout << "List: ";
        getline(cin, input);
        input = formatString(input, true);

        if(input == "1" || input == "Scientists")
            optionTwo = "gender", optionThree = "birth", optionFour = "death", type = "computerScientist";

        else if(input == "2" || input == "Computers")
            optionTwo = "type", optionThree = "build year", optionFour = "built", type = "computer";

        else
        {
            valid = false;
            cout << "\nInvalid Input.\n\n";
        }

    }while(!valid);

    do
    {
        valid = true;
        cout << "\nHow do you want to display the list?:\n\n";
        cout << "\t1:" << optionOne << "\n\t2:" << optionTwo << "\n\t3:" << optionThree << "\n\t4:" << optionFour << "\n\n";
        cout << "display type: ";

        getline(cin, input);
        input = formatString(input,true);


        if (input == formatString(optionOne, true) || input == "1"){
            displayCase = 1;
        }

        else if (input == formatString(optionTwo, true) || input == "2"){
            displayCase = 2;
        }

        else if (input == formatString(optionThree, true) || input == "3"){
            displayCase = 3;
        }

        else if (input == formatString(optionFour, true) || input == "4"){
            displayCase = 4;
        }

        else{
            cout << "\nInvalid input" << endl;
            valid = false;
        }

    }while(!valid);

    serviceLayer.getSortedList(type, displayCase);

    if(type == "computerScientist")
    {
        vector<computerScientist> scientistList = serviceLayer.getScientistList();
        didPrint = printListOfScientist(scientistList);

        if(!didPrint)
            cout << "The list is empty. Please add computer scientists to it!\n\n";
    }
    else
    {
        vector<computers> computerList = serviceLayer.getComputerList();
        didPrint = printListOfComputer(computerList);

        if(!didPrint)
            cout << "The list is empty. Please add computer computers to it!\n\n";
    }
}

void consoleUI::processRelation()
{
    string input;
    vector<computerScientist> scientistList;
    vector<computers> computerList;
    int sciSize,compSize;
    bool isSee;

    cout << "\nPlease enter the name (or a part of the name) of the scientist/computer"
            "\nyou want to do relation operations on.\n\n";
    cout << "Name: ";

    getline(cin, input);
    input = formatString(input, false);

    if(input == "")
    {
        cout << "\nNo scientist/computer found.\n\n";
        return;
    }

    serviceLayer.SearchByColumn(input,"name");
    scientistList = serviceLayer.getScientistList();
    computerList = serviceLayer.getComputerList();

    sciSize = scientistList.size();
    compSize = computerList.size();

    cout << endl << sciSize << " scientists and " << compSize << " computers have been found by that name.\n\n";

    if(sciSize)
    {
        cout << "Do you want to see the scientists(Y/N)?:";
        getline(cin,input);
        input = formatString(input, true);

        if(input == "Y")
            isSee = true;
        else
            isSee = false;
    }

    if(isSee)
    {
        isSee = false;

        for(int i = 0; i < sciSize; i++)
        {
            cout << "\nWrite a relation command below the entry.\n";
            cout << "The following operations can be made.\n\n";
            cout << "\tadd\tAdd a relation to the entry\n\tview\tView all the relations the entry has\n\n";

            cout << "-----------------------------\n";
            cout << "\tScientist\n\n";
            cout << scientistList[i];
            cout << "-----------------------------\n";
            cout << ": ";
            getline(cin,input);
            input = formatString(input,true);

            if(input == "Add")
                addRelationToScientist(scientistList[i].getID());

            else if(input == "View")
                viewScientistsRelations(scientistList[i]);

            if(i < sciSize - 1)
            {
                cout << "\nPress Enter to get the next Scientist\n";
                getline(cin,input);
            }
        }
    }

    if(compSize)
    {
        cout << "\nDo you want to see the computers?(Y/N)?:";
        getline(cin,input);
        input = formatString(input, true);

        if(input == "Y")
            isSee = true;
        else
            isSee = false;
    }

    if(isSee)
    {
        for(int i = 0; i < compSize; i++)
        {
            cout << "\nWrite a relation command below the entry.\n";
            cout << "The following operations can be made.\n\n";
            cout << "\tadd\tAdd a relation to the entry\n\tview\tView all the relations the entry has\n\n";

            cout << "-----------------------------\n";
            cout << "\tComputer\n\n";
            cout << computerList[i];
            cout << "-----------------------------\n";
            cout << ": ";
            getline(cin,input);
            input = formatString(input, true);

            if(input == "Add")
                addRelationToComputer(computerList[i].getID());
            else if(input == "View")
                viewComputerRelations(computerList[i]);

            if(i < compSize - 1)
            {
                cout << "\nPress Enter to get the next Computer\n";
                getline(cin,input);
            }
        }
    }
}

void consoleUI::addRelationToScientist(const int& sciID)
{
    string input;
    vector<computers> list;

    cout << "\nInsert the name of the computer you want to relate to the scientist\n\n";
    cout <<"Name: ";
    getline(cin,input);
    input = formatString(input,false);

    if(input == "")
    {
        cout << "\nNo computer found.\n\n";
        return;
    }

    serviceLayer.SearchByColumn(input,"name");
    list = serviceLayer.getComputerList();

    if(!list.size())
        cout << "\nNo computers found by that name\n\n";
    else
    {
        int listSize = list.size();
        cout << endl << listSize << " computers found by that name.\n\n";
        cout << "\nWrite \"add\" to add relation, anything else will fetch the next computer.\n\n";

        for(int i = 0; i < listSize; i++)
        {
            cout << "-----------------------------\n";
            cout << "\tComputer\n\n";
            cout << list[i];
            cout << "-----------------------------\n";
            cout << ": ";
            getline(cin,input);
            input = formatString(input,true);

            if(input == "Add")
                serviceLayer.addRelation(sciID, list[i].getID());
        }
    }
}

void consoleUI::addRelationToComputer(const int& compID)
{
    string input;
    vector<computerScientist> list;

    cout << "\nInsert the name of the Scientist you want to relate to the computer\n\n";
    cout <<"Name: ";
    getline(cin,input);

    if(input == "")
    {
        cout << "\nNo scientits found.\n\n";
        return;
    }

    input = formatString(input,false);
    serviceLayer.SearchByColumn(input,"name");
    list = serviceLayer.getScientistList();

    if(!list.size())
        cout << "\nNo scientists found by that name\n\n";
    else
    {
        int listSize = list.size();
        cout << endl << listSize << " scientists found by that name.\n\n";
        cout << "\nWrite \"add\" to add relation, anything else will fetch the next scientist.\n\n";

        for(int i = 0; i < listSize; i++)
        {
            cout << "-----------------------------\n";
            cout << "\tScientist\n\n";
            cout << list[i];
            cout << "-----------------------------\n:";
            getline(cin,input);
            input = formatString(input,true);

            if(input == "Add")
                serviceLayer.addRelation(list[i].getID(),compID);
        }
    }
}

void consoleUI::viewScientistsRelations(computerScientist sci)
{
    string input;
    serviceLayer.scientistRelations(sci.getID());
    vector<computers> computerList = serviceLayer.getComputerList();
    int listSize = computerList.size();

    if(!computerList.size())
        cout << "\nThis scientist is not related to any computers.\n\n";
    else
    {
        cout << "\nThe following " << listSize << " computers are related to " << sci.getName() << endl;
        cout << "\nIf you want to remove any relation write \"remove\", anything else will fetch the next computer.\n\n";

        for(int i = 0; i < listSize; i++)
        {
            cout << "-----------------------------\n";
            cout << computerList[i];
            cout << "-----------------------------\n:";
            getline(cin,input);
            input = formatString(input,true);

            if(input == "Remove")
                serviceLayer.removeRelation(sci.getID(), computerList[i].getID());
        }
    }
}

void consoleUI::viewComputerRelations(computers comp)
{
    string input;
    serviceLayer.computerRelations(comp.getID());
    vector<computerScientist> scientistList = serviceLayer.getScientistList();
    int listSize = scientistList.size();

    if(!listSize)
        cout << "\nThis computer is not related to any scientists.\n\n";
    else
    {
        cout << "\nThe following " << listSize << " scientists are related to " << comp.getName() << endl;
        cout << "\nIf you want to remove any relation write \"remove\", anything else will fetch the next scientist.\n\n";

        for(int i = 0; i < listSize; i++)
        {
            cout << "-----------------------------\n";
            cout << scientistList[i];
            cout << "-----------------------------\n:";
            getline(cin,input);
            input = formatString(input,true);

            if(input == "Remove")
                serviceLayer.removeRelation(scientistList[i].getID(), comp.getID());
        }
    }
}

void makeScientist(computerScientist& entry)
{
    string name, gender;
    int birthYear, deathYear;

    cout << "\nPlease insert the following information about the computer scientist:\n\n";
    cout << "\t(*):required information\n";
    cout << "\tYou can skip entering unneccesery information\n\tby hitting the Enter button.\n\n";

    name = askForName(false, false);
    name = formatString(name, true);
    gender = askForGender();
    birthYear = askForBirthYear();
    deathYear = askForDeathYear(birthYear);

    entry.edit(name,gender,birthYear,deathYear);
}

void makeComputer(computers& entry)
{
    string name, type;
    int buildYear;
    bool wasBuilt;

    cout << "\nPlease insert the following information about the computer:\n\n";
    cout << "\t(*):required information\n";
    cout << "\tYou can skip entering unneccesery information\n\tby hitting the Enter button.\n\n";

    name = askForName(true, true);
    name = formatString(name, false);
    type = askForType();
    buildYear = askForBuildYear();
    wasBuilt = askForWasBuilt();

    entry.edit(name, buildYear, type, wasBuilt);
}

string formatString(const string& str, const bool& isFormatUpperLower)
{
    int stringLength = str.length();
    bool lastCharSpace = true;
    bool anyNonSpace = false;

    string newString = "";

    for(int i = 0; i < stringLength; i++)
    {
        if(isspace(str[i]))
            lastCharSpace = true;

        else if(isalpha(str[i]) || isdigit(str[i]) || ispunct(str[i]))
        {
            if(lastCharSpace)
            {//If last character was a space
                if(anyNonSpace)//If any none space has appeared in the string before
                    newString += " ";//Add a space between words
                else
                    anyNonSpace = true;//Dont make space before the first word

                if(isFormatUpperLower)//If character formating is true
                    newString += toupper(str[i]);//set the first letter in the word to uppercase

                else//Have the character like the user inserted it
                    newString += str[i];

                lastCharSpace = false;
            }
            else
            {//If not the first character in a word
                if(isFormatUpperLower)
                    newString += tolower(str[i]);
                else
                    newString +=  str[i];
            }
        }
    }

    return newString;
}

bool isNameValid(const string& name, const bool& allowDigits, const bool& allowPunct)
{
    int nameLength = name.length();
    int alphaCount = 0;

    for(int i = 0; i < nameLength; i++)
    {
        if(isalpha(name[i]) || allowDigits * isdigit(name[i]) || allowPunct * ispunct(name[i]))
            alphaCount++;

        else if(!isspace(name[i]))
        {
            cout << "\nInvalid characters in input.\n";
            return false;
        }
    }

    if(alphaCount > 0)
        return true;
    else
        return false;
}

bool isYearValid(const string& year)
{
    int stringLength = year.length();

    for(int i = 0; i < stringLength; i++)
    {
        if(!isdigit(year[i]))
        {
            cout << "\nYears must be represented only as a positive integer.\n";
            return false;
        }
    }

    return true;
}

bool printListOfScientist(vector<computerScientist>& list)
{
    int listSize = list.size();
    string line = "-----------------------------\n";

    cout << endl;

    if(listSize < 1)
        return false;

    cout << "\tScientists\n";
    cout << line;

    for(int i = 0; i < listSize; i++)
    {
        cout << list[i];
        cout << line;
    }

    cout << endl;

    return true;
}

bool printListOfComputer(vector<computers>& list)
{
    int listSize = list.size();
    string line = "-----------------------------\n";

    cout << endl;

    if(listSize < 1)
        return false;

    cout << "\tComputers\n";
    cout << line;

    for(int i = 0; i < listSize; i++)
    {
        cout << list[i];
        cout << line;
    }

    cout << endl;

    return true;
}

string askForName(const bool& allowDigits, const bool& allowPunct)
{
    bool isValid;
    string name;

    do
    {
        cout << "*Name: ";
        getline(cin, name);

        isValid = isNameValid(name, allowDigits, allowPunct);

        if(!isValid)
            cout << "\nWrong Input, please enter again.\n\n";

    }while(!isValid);

    return name;
}

string askForGender()
{
    bool isValid;
    string gender;

    do
    {
        cout << "*Gender(Male/Female): ";
        getline(cin, gender);
        gender = formatString(gender, true);

        if(gender != "Male" && gender != "Female")
        {
            isValid = false;
            cout << "\nWrong Input, please enter again.\n\n";
        }
        else
            isValid = true;

    }while(!isValid);

    return gender;
}

string askForType()
{
    bool isValid;
    string type;

    do
    {
        cout << "Type: ";
        getline(cin, type);
        type = formatString(type, true);

        if(type.length())
            isValid = isNameValid(type, false, false);
        else
            isValid = true;

        if(!isValid)
            cout << "\nWrong Input, please enter again.\n\n";

    }while(!isValid);

    return type;
}

int askForBirthYear()
{
    bool isValid;
    int birthYear;

    do
    {
        cout << "Year of birth: ";
        isValid = makeYear(birthYear);

        if(!isValid)
            cout << "\nWrong Input, please enter again.\n\n";

    }while(!isValid);

    return birthYear;
}

int askForDeathYear(const int& birthYear)
{
    bool isValid;
    int deathYear;

    do
    {
        cout << "Year of death: ";
        isValid = makeYear(deathYear);
        isValid = true;

        if((deathYear < birthYear) && deathYear != -1)
        {
            isValid = false;
            cout << "\nA person cannot die before she is born.\n";
            cout << "\nWrong Input, please enter again.\n\n";
        }

    }while(!isValid);

    return deathYear;
}

int askForBuildYear()
{
    bool isValid;
    int buildYear;

    do
    {
        cout << "What year was the computer built?: ";
        isValid = makeYear(buildYear);

        if(!isValid)
            cout << "\nWrong Input, please enter again.\n\n";
    }while(!isValid);

    return buildYear;
}

bool askForWasBuilt()
{
    bool wasBuilt, isValid = false;
    string input;

    do
    {
        cout << "*Was the computer ever built?(Y/N): ";
        getline(cin, input);

        if(input == "y" || input == "Y")
        {
            wasBuilt = true;
            isValid = true;
        }

        else if(input == "n" || input == "N")
        {
            wasBuilt = false;
            isValid = true;
        }
        else
            cout << "\nInvalid input.\n\n";

    }while(!isValid);

    return wasBuilt;
}

bool makeYear(int& year)
{
    string tempYear;
    bool isValid;

    getline(cin, tempYear);

    isValid = isYearValid(tempYear);

    if(tempYear == "")
        tempYear = "-1";

    year = atoi(tempYear.c_str());

    return isValid;
}

void editScientist(computerScientist& entry)
{
    string name, gender, input;
    int birthYear, deathYear;
    bool again;

    name = entry.getName();
    gender = entry.getGender();
    birthYear = entry.getBirthYear();
    deathYear = entry.getDeathYear();

    do{
        editScientistInfo(name, gender, birthYear, deathYear);
        entry.edit(name,gender,birthYear,deathYear);

        cout << "\nDo you want to continue editing this scientist(Y/N)?:";
        getline(cin,input);

        if(input == "Y" || input == "y")
            again = true;

        else
            again = false;

    }while(again);
}

void editComputer(computers& entry)
{
    string name, type, input;
    int buildYear;
    bool wasBuilt, again;

    name = entry.getName();
    type = entry.getType();
    buildYear = entry.getBuildYear();
    wasBuilt = entry.getWasBuilt();

    do{

        editComputerInfo(name, type, buildYear, wasBuilt);
        entry.edit(name, buildYear, type, wasBuilt);

        cout << "\nDo you want to continue editing this computer(Y/N)?:";
        getline(cin,input);

        if(input == "Y" || input == "y")
            again = true;

        else
            again = false;

    }while(again);
}

void editScientistInfo(string& name, string& gender, int& birthYear, int& deathYear)
{
    string input;

        cout << "What do you want to edit?\n\n\t1:Name\n\t2:Gender\n\t3:Birth year\n\t4:Death year\n\n";
        cout << "Edit: ";

        getline(cin,input);

        cout << endl;

        if (input == "name" || input == "Name" || input == "1"){
            name = askForName(false, false);
            name = formatString(name,true);
        }

        else if (input == "gender" || input == "Gender" || input == "2"){
            gender = askForGender();
        }

        else if (input == "birth year" || input == "Birth year" || input == "3"){
            birthYear = askForBirthYear();
        }

        else if (input == "death year" || input == "Death year" || input == "4"){
            deathYear = askForDeathYear(birthYear);
        }

        else
            cout << "invalid input.\n\n";
}

void editComputerInfo(string& name, string& type, int& buildYear, bool& wasBuilt)
{
    string input;

        cout << "What do you want to edit?\n\n\t1:Name\n\t2:Type\n\t3:Build year\n\t4:Was built\n\n";
        cout << "Edit: ";

        getline(cin,input);

        cout << endl;

        if (input == "name" || input == "Name" || input == "1"){
            name = askForName(true, true);
            name = formatString(name, false);
        }

        else if (input == "type" || input == "Type" || input == "2"){
            type = askForType();
        }

        else if (input == "build year" || input == "Build year" || input == "3"){
            buildYear = askForBuildYear();
        }

        else if (input == "was built" || input == "Was built" || input == "4"){
            wasBuilt = askForWasBuilt();
        }

        else
            cout << "invalid input.\n\n";
}

ostream& operator << (ostream& out, computerScientist a)
{
    out << "Name: " << "\t\t" << a.getName() << endl;
    out << "Gender: "<< "\t" << a.getGender() << endl;

    if(a.getBirthYear() != ERROR)
        out << "Birth Year: " << "\t" << a.getBirthYear() << endl;
    else
        out << "Birth Year: " << "\t" << "N/A\n";

    if(a.getDeathYear() != ERROR)
        out << "Death year: " << "\t" << a.getDeathYear() << endl;
    else
        out << "Death year: " << "\t" << "N/A\n";

    return out;
}

ostream& operator << (ostream& out, computers c)
{
    out << "Name: " << "\t\t" << c.getName() << endl;

    if(!c.getType().length())
        out << "Type: "<< "\t\t" << "N/A\n";
    else
        out << "Type: "<< "\t\t" << c.getType() << endl;

    if(c.getBuildYear() != ERROR)
        out << "Build Year: " << "\t" << c.getBuildYear() << endl;
    else
        out << "Build Year: " << "\t" << "N/A\n";

    out << "Was built: " << "\t";

    if(c.getWasBuilt())
        out << "Yes" << endl;
    else
        out << "No" << endl;

    return out;
}
