#ifndef COMPUTERSCIENTIST_H
#define COMPUTERSCIENTIST_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class computerScientist
{
public:
    computerScientist();
    computerScientist(const string &sName, const string &sGender, const int &bYear, const int &dYear, const int &sID, const bool &isDeleted);
    string getName();
    string getGender();
    int getBirthYear();
    int getDeathYear();
    int getID();
    void edit(const string &sName, const string &sGender, const int &sBirthYear, const int &sDeathYear);
    bool isDeleted();
private:
    string name;
    string gender;
    int birthYear;
    int deathYear;
    int ID;
    bool deleted;
};

#endif // COMPUTERSCIENTIST_H
