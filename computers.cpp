#include "computers.h"

computers::computers()
{
    name = "";
    buildYear = -1;
    type = "";
    wasBuilt = 0;
    ID = 0;
}

computers::computers(const string& name, const string& type, const int& buildYear, const bool& wasBuilt , const int& cID, const bool& isDeleted)
{
    edit(name, buildYear, type, wasBuilt);
    ID = cID;
    deleted = isDeleted;
}

string computers::getName()
{
    return name;
}

int computers::getBuildYear()
{
    return buildYear;
}

bool computers::getWasBuilt()
{
    return wasBuilt;
}

string computers::getType()
{
    return type;
}

int computers::getID()
{
    return ID;
}

bool computers::isDeleted()
{
    return deleted;
}

void computers::edit(const string& c_name, const int& c_buildYear, const string& c_type, const bool& c_wasBuilt)
{
    name = c_name;
    buildYear = c_buildYear;
    type = c_type;
    wasBuilt = c_wasBuilt;
}
