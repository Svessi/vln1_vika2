#ifndef LISTSERVICE_H
#define LISTSERVICE_H

#include <iostream>
#include "sqlrepository.h"
#include "computers.h"

using namespace std;

class listService
{
public:
    listService();
    bool addScientist(computerScientist &entry);
    bool addComputer(computers& entry);
    void remove(const string &type, const int &ID);
    void getSortedList(const string &type, const int &display_case);
    void searchResults(const string &input);
    vector<computerScientist> getScientistList();
    vector<computers> getComputerList();
    void SearchByColumn(const string &input, const string &tableCol);
    void editScientist(const computerScientist &entry);
    void editComputer(const computers &entry);
    void addRelation(const int &scientistID, const int &computerID);
    void removeRelation(const int &scientistID, const int &computerID);
    void computerRelations(const int &computerID);
    void scientistRelations(const int &scientistID);
private:
    sqlrepository repository;
};

vector<string> splitString(const string& str);
bool isSubString(const string& str, const string& subStr);
bool genderCompare(computerScientist s1, computerScientist s2);
bool nameCompare(computerScientist s1, computerScientist s2);
bool birthCompare(computerScientist s1, computerScientist s2);
bool deathCompare(computerScientist s1, computerScientist s2);


#endif // LISTSERVICE_H
