#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T21:28:34
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = computerScientists
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    computerscientist.cpp \
    consoleui.cpp \
    listservice.cpp \
    sqlrepository.cpp \
    computers.cpp

HEADERS += \
    computerscientist.h \
    consoleui.h \
    listservice.h \
    sqlrepository.h \
    computers.h

OTHER_FILES += \
    commands.txt
