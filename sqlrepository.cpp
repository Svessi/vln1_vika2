#include "sqlrepository.h"

sqlrepository::sqlrepository()
{
    dbName = "computerScience.sqlite";
    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(dbName);
    database.open();
}

sqlrepository::~sqlrepository()
{
    database.close();
    QSqlDatabase::removeDatabase(dbName);
}

vector<computerScientist> sqlrepository::getScientistList()
{
    return scientistList;
}

vector<computers> sqlrepository::getComputerList()
{
    return computerList;
}

void sqlrepository::findInRepository(const vector<string>& input)
{//Finds anything in the database that is LIKE an element in the vector of search words(input)

    vector<string> tempInput = input;
    addWildCarts(tempInput);//Add wildcarts on both side of each input

    vector<string> col;
    col = getAllScientistCol(); //Get all the columns that the computerScientist table has

    string command = makeSearchCommand("computerScientist", col, tempInput);//search command that will search for every input in every table column
    readSientistsFromDatabase(command);

    col = getAllComputerCol();
    command = makeSearchCommand("computer", col, tempInput);
    readComputersFromDatabase(command);
}

void sqlrepository::findScientistInRepository(computerScientist sci)
{
    string name, gender, birthYear, deathYear, command;

    //Get all the info from entry
    name = sci.getName();
    gender = sci.getGender();
    birthYear = IntToString(sci.getBirthYear());
    deathYear = IntToString(sci.getDeathYear());

    command = "SELECT * FROM computerScientist s WHERE s.name = \"" + name
              + "\" AND s.gender = \"" + gender + "\" AND s.birthYear = "
              + birthYear + " AND s.deathYear = " + deathYear + ";";

    readSientistsFromDatabase(command);//If found the entry should be in the the scientistList
}

void sqlrepository::findComputerInRepository(computers comp)
{
    string name, type, buildYear, wasBuilt, command;

    //Get all the info from the entry
    name = comp.getName();
    type = comp.getType();
    buildYear = IntToString(comp.getBuildYear());
    wasBuilt = IntToString(comp.getWasBuilt());

    command = "SELECT * FROM computer c WHERE c.name = \"" + name
              + "\" AND c.type = \"" + type + "\" AND c.buildYear = "
              + buildYear + " AND c.wasBuilt = " + wasBuilt + ";";

    readComputersFromDatabase(command);
}

void sqlrepository::findByColumn(const string& input, const string& tableCol)
{
    string command;

    if(tableCol == "name" || tableCol == "gender" || tableCol == "birthYear" || tableCol == "deathYear")
    {
        command = "SELECT * FROM computerScientist s WHERE s.isDeleted = 0 AND s." + tableCol +" LIKE \"%" + input + "%\";";
        readSientistsFromDatabase(command);
    }

    if(tableCol == "name" || tableCol == "type" || tableCol == "buildYear" || tableCol == "wasBuilt")
    {
        command = "SELECT * FROM computer c WHERE c.isDeleted = 0 AND c." + tableCol +" LIKE \"%" + input + "%\";";
        readComputersFromDatabase(command);
    }
}

bool sqlrepository::addScientist(computerScientist entry)
{
    findScientistInRepository(entry);

    if(!(scientistList.size()))
    {//ENTRY DOES NOT EXIST IN THE DATABASE(INSERT)
        insertScientist(entry);
    }
    else
    {//ENTRY DOES EXIST IN THE DATABASE(UPDATE)
        computerScientist temp = scientistList[0];
        bool isDeleted = temp.isDeleted();

        if(isDeleted)
        {
            int ID = temp.getID();
            restoreOrDelete("computerScientist", ID, 0);
        }
        else
            return false; //The scientist is in the database and is not deleted, cannot be added again.
    }

    return true;
}

bool sqlrepository::addComputer(computers entry)
{
    findComputerInRepository(entry);//is the computer already in the database?

    if(!(computerList.size()))
    {//ENTRY DOES NOT EXIST IN THE DATABASE(INSERT)
        insertComputer(entry);
    }
    else
    {//ENTRY DOES EXIST IN THE DATABASE(UPDATE)
        computers temp = computerList[0];
        bool isDeleted = temp.isDeleted();

        if(isDeleted)
        {//If the entry is marked as deleted in the database
            int ID = temp.getID();
            restoreOrDelete("computer", ID, 0);//Mark the entry as not deleted in the database
        }
        else
            return false;//The computer is already in the database and is not deleted, cannot be added again.
    }

    return true;
}

void sqlrepository::insertScientist(computerScientist entry)
{
    QString qName, qGender, qBirthYear, qDeathYear;

    QSqlQuery query(database);

    qName = entry.getName().c_str();
    qGender = entry.getGender().c_str();
    qBirthYear = IntToString(entry.getBirthYear()).c_str();
    qDeathYear = IntToString(entry.getDeathYear()).c_str();


    query.prepare("INSERT INTO computerScientist (name, gender, birthYear, deathYear)"
                  "VALUES (:name, :gender, :birthYear, :deathYear)");

    query.bindValue(":name", qName);
    query.bindValue(":gender", qGender);
    query.bindValue(":birthYear", qBirthYear);
    query.bindValue(":deathYear", qDeathYear);

    query.exec();
}

void sqlrepository::insertComputer(computers entry)
{
    QString qName, qType, qBuildYear, qWasBuilt;

    QSqlQuery query(database);

    qName = entry.getName().c_str();
    qType = entry.getType().c_str();
    qBuildYear = IntToString(entry.getBuildYear()).c_str();
    qWasBuilt = IntToString(entry.getWasBuilt()).c_str();

    query.prepare("INSERT INTO computer (name, type, buildYear, wasBuilt)"
                  "VALUES (:name, :type, :buildYear, :wasBuilt)");

    query.bindValue(":name", qName);
    query.bindValue(":type", qType);
    query.bindValue(":buildYear", qBuildYear);
    query.bindValue(":wasBuilt", qWasBuilt);

    query.exec();

}

void sqlrepository::updateScientist(const int& ID, const string& name, const string& gender, const int& birthY, const int& deathY)
{
    QString qID, qName, qGender, qBirthYear, qDeathYear;
    QSqlQuery query(database);

    qID = IntToString(ID).c_str();
    qName = name.c_str();
    qGender = gender.c_str();
    qBirthYear = IntToString(birthY).c_str();
    qDeathYear = IntToString(deathY).c_str();

    query.exec("UPDATE computerScientist SET name = \"" + qName + "\", gender = \""
               + qGender + "\", birthYear = " + qBirthYear + ", deathYear = "
               + qDeathYear + " Where ID = " + qID + ";");
}

void sqlrepository::updateComputer(const int& ID, const string& name, const string& type, const int& buildY, const bool& wasBuilt)
{
    QString qID, qName, qType, qBuildYear, qWasBuilt;
    QSqlQuery query(database);

    qID = IntToString(ID).c_str();
    qName = name.c_str();
    qType = type.c_str();
    qBuildYear = IntToString(buildY).c_str();
    qWasBuilt = IntToString(wasBuilt).c_str();

    query.exec("UPDATE computer SET name = \"" + qName + "\", type = \""
               + qType + "\", buildYear = " + qBuildYear + ", wasBuilt = "
               + qWasBuilt + " Where ID = " + qID + ";");
}

void sqlrepository::restoreOrDelete(const string& type, const int& ID, const bool& isDelete)
{
    QString qType, qID, qIsDelete;
    QSqlQuery query(database);
    string sIsDelete = IntToString(isDelete);
    string sID = IntToString(ID);
    qType = type.c_str();
    qID = sID.c_str();
    qIsDelete = sIsDelete.c_str();

    query.exec("UPDATE " + qType + " SET isDeleted = " + qIsDelete + " WHERE ID = " + qID + ";");

    if(isDelete)
    {
        if(type == "computerScientist")
            query.exec("DELETE FROM relations WHERE scientistID = " + qID);

        else if(type == "computer")
            query.exec("DELETE FROM relations WHERE computerID = " + qID);
    }
}

void sqlrepository::readSientistsFromDatabase(const string& command)
{
    string tName, tGender;
    int tBirth, tDeath, tID;
    bool isDeleted;
    QString qCommand = command.c_str();
    QSqlQuery query(database);
    vector<computerScientist> init;
    scientistList = init;

    if(!database.isOpen())
        database.open();

    query.exec(qCommand);

    while(query.next())
    {
        tID = query.value("ID").toInt();
        tName = query.value("name").toString().toStdString();
        tGender = query.value("gender").toString().toStdString();
        tBirth = query.value("birthYear").toInt();
        tDeath = query.value("deathYear").toInt();
        isDeleted = query.value("isDeleted").toBool();
        computerScientist s(tName, tGender, tBirth, tDeath, tID, isDeleted);
        scientistList.push_back(s);
    }
}

void sqlrepository::readComputersFromDatabase(const string& command)
{
    string tName, tType;
    int tBuildYear, tID;
    bool tWasBuilt, isDeleted;
    QString qCommand = command.c_str();
    QSqlQuery query(database);
    vector<computers> init;
    computerList = init;

    if(!database.isOpen())
        database.open();

    query.exec(qCommand);

    while(query.next())
    {
        tID = query.value("ID").toInt();
        tName = query.value("name").toString().toStdString();
        tType = query.value("type").toString().toStdString();
        tBuildYear = query.value("buildYear").toInt();
        tWasBuilt = query.value("wasBuilt").toBool();
        isDeleted = query.value("isDeleted").toBool();
        computers c(tName, tType,tBuildYear, tWasBuilt, tID, isDeleted);
        computerList.push_back(c);
    }
}

void sqlrepository::sort(const string& tableName, const string& colName)
{
    string command = "SELECT * FROM " + tableName + " s WHERE s.isDeleted = 0 "
                     + "ORDER BY " + colName + " COLLATE NOCASE;";

    if(tableName == "computerScientist")
        readSientistsFromDatabase(command);

    else if(tableName == "computer")
        readComputersFromDatabase(command);
}

void sqlrepository::scientistRelation(int ID)
{
    string command = "SELECT c.* FROM computer c INNER JOIN relations r ON r.scientistID = "
                     + IntToString(ID) + " WHERE r.computerID = c.ID AND c.isDeleted = 0;";

    readComputersFromDatabase(command);
}

void sqlrepository::computerRelation(int ID)
{
    string command = "SELECT s.* FROM computerScientist s INNER JOIN relations r ON r.computerID = "
                    + IntToString(ID) + " WHERE r.scientistID = s.ID AND s.isDeleted = 0;";

    readSientistsFromDatabase(command);
}

void sqlrepository::addRelation(const int& scientistID, const int& computerID)
{
    QString sciID, compID;
    QSqlQuery query(database);

    sciID = IntToString(scientistID).c_str();
    compID = IntToString(computerID).c_str();

    query.prepare("INSERT INTO relations (scientistID,computerID)"
                  "VALUES (:scientistID, :computerID)");

    query.bindValue(":scientistID", sciID);
    query.bindValue(":computerID", compID);
    query.exec();
}

void sqlrepository::removeRelation(const int& scientistID, const int& computerID)
{
    QString sciID, compID;
    QSqlQuery query(database);

    sciID = IntToString(scientistID).c_str();
    compID = IntToString(computerID).c_str();

    query.exec("DELETE FROM relations WHERE scientistID = " + sciID + " AND computerID = " + compID);
}

string IntToString(const int& integer)
{
    string str;
    stringstream out;
    out << integer;
    str = out.str();
    return str;
}

string makeSearchCommand(const string& type, const vector<string>& col, const vector<string>& search)
{
    int searchSize = search.size();
    int colSize = col.size();
    string command = "SELECT * FROM " + type + " s WHERE s.isDeleted = 0 AND s.";

    for(int i = 0; i < searchSize; i++)
    {
        for(int j = 0; j < colSize; j++)
        {
            if(i > 0 || j > 0)
                command += " OR s.";

            command += col[j] + " LIKE \"" + search[i] + "\"";
        }
    }
    return command;
}

void addWildCarts(vector<string>& list)
{
    int size = list.size();

    for(int i = 0; i < size; i++)
    {
        list[i] = "%" + list[i] + "%";
    }
}

vector<string> getAllScientistCol()
{
    vector<string> col;
    col.push_back("name");
    col.push_back("gender");
    col.push_back("birthYear");
    col.push_back("deathYear");
    return col;
}

vector<string> getAllComputerCol()
{
    vector<string> col;
    col.push_back("name");
    col.push_back("type");
    col.push_back("buildYear");
    col.push_back("wasBuilt");
    return col;
}
